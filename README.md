# TrcScoutingApp
An Android app for FIRST Tech Challenge scouting.

# How To Use
- Phone/Tablet: Import this entire folder into Android Studio or Eclipse w/ Android build plugins, and hit build. Then use ADB to sideload the apk onto the tablet. Or you can build it using Gradle v3 or above.

# Changelog
v1.3
- Software has been majorly unsucked.
- Glitchy save system rewritten, and active sessions will be remembered for offline use.
- Made menus more intuitive.

# Supported platforms
- Android 4.3 JellyBean or above 
